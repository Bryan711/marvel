//
//  ViewController.swift
//  MARVEL
//
//  Created by BRYAN OCAÑA on 28/11/17.
//  Copyright © 2017 BRYAN OCAÑA. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    //MARK:-Outl
    @IBOutlet weak var NombreLabel: UILabel!
    
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func ConultarButton(_ sender: Any) {
        
        let pkid = arc4random_uniform(250) + 1
        let URL = "https://pokeapi.co/api/v2/pokemon/\(pkid)/"
        
        Alamofire.request(URL).responseObject { (response: DataResponse<Pokemon>) in
            
            let pokemon = response.result.value
            DispatchQueue.main.async {
                self.NombreLabel.text = pokemon?.name ?? ""
                self.heightLabel.text = "\(pokemon?.height ?? 0)"
                self.weightLabel.text = "\(pokemon?.weight ?? 0)"
            }
        }
    }
    
}

